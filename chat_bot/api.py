from . import base_url

import json
import requests

from typing import Optional
from traceback import print_exc


def get_updates(offset=None) -> Optional[list]:
    url = base_url + 'getUpdates'

    try:
        if offset:
            response = requests.get(url, params={'offset': offset})
        else:
            response = requests.get(url)

        if response.status_code == 200:
            return response.json()['result']

    except Exception as e:
        print_exc()

    return None


def send_message(chat_id, text, **params) -> bool:
    url = base_url + 'sendMessage'

    params.update({
        'chat_id': chat_id,
        'text': text
    })

    try:
        response = requests.post(url, data=params)

        if response.status_code != 200:
            print(json.loads(response.content.decode())['description'])

        return response.status_code == 200

    except Exception as e:
        print_exc()

    return False
