"""
V10. Telegram Bot - birthday reminder

bot username: @ais_v10_bot
link: t.me/ais_v10_bot
"""

__all__ = ['base_url', 'data_handler', 'dispatcher']
import os

_dir = os.path.dirname(os.path.abspath(__file__))
_data_path = os.path.join(_dir, 'data.json')
_token_path = os.path.join(_dir, 'token')

try:
    with open(_token_path, 'r', encoding='utf-8') as file:
        token = file.read().strip()

except FileNotFoundError:

    token = os.getenv('BOT_TOKEN')
    if not token:
        print('NO TOKEN')
        exit(-1)

base_url = f'https://api.telegram.org/bot{token}/'

from .datahandler import DataHandler

data_handler = DataHandler(_data_path)

from .dispatcher import Dispatcher

dispatcher = Dispatcher()
