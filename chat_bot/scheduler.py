from . import api
import schedule

from datetime import datetime, date


def register(data, data_handler):
    """Register jobs to be called regularly"""
    print('Register scheduled jobs')

    def notify():
        """
        Notify users of upcoming birthdays
        """
        cd = date.today()

        for user in data['users'].values():

            birthdays = []

            for birthday in user['dates']:

                bd = datetime.strptime(birthday['date'],
                                       data['config']['date_format']).date()

                if (bd.replace(year=cd.year) - cd).days == 2:
                    birthdays.append(birthday)

            if birthdays:
                new_bd = date.today().replace(day=date.today().day + 2)

                if len(birthdays) == 1:
                    birthday = birthdays[0]

                    text = '{person} is having his birthday just in 2 days, ' \
                           f'{new_bd:%d.%m.%Y}!'

                    if birthday['address']:
                        text += ' He will be celebrating it at {address}.'

                    text = text.format(**birthday)

                else:
                    text = 'Great news! These people are having birthday in ' \
                           f'just 2 days, {new_bd:%d.%m.%Y}:\n'

                    for i, birthday in enumerate(birthdays):
                        text += f'\n{i + 1}. ''{person}'

                        if birthday['address']:
                            text += ' ({address})'

                        text = text.format(**birthday)

                api.send_message(user['id'], text)

    def save():
        """
        Save data to file
        """
        print('Save')
        data_handler.save()

    # Register jobs
    schedule.every().day.at('10:00').do(notify)
    schedule.every(10).minutes.do(save)
