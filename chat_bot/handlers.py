from . import functions
from .message import Message

from datetime import datetime


def register(data, dispatcher):
    """
    Register handlers for commands received by bot
    :param data: data dictionary
    :param dispatcher: message dispatcher
    """
    print('Register message handlers')

    @dispatcher.handler('start')
    def new_user(message: Message):
        if str(message.from_user.id) in data['users']:
            reply = f'Hey! I already know you!'

        else:
            reply = f'Hello, {message.from_user.first_name} ' \
                    f'{message.from_user.last_name}!\n\n' \
                    f'If you don\'t know what to do, type /help'

            u = functions.make_user(message.from_user.id,
                                    message.from_user.username)

            data['users'][str(message.from_user.id)] = u

        message.answer(reply)

    @dispatcher.handler('help')
    def send_help(message: Message):
        message.answer(
            'I am a bot who will remember all your friends\' birthdays so you '
            'don\'t have to :). Here are my commands:\n\n'
            '/start - Will let me start working with you '
            '(if I haven\'t already)\n' 
            '/help - I\'ll send you this message once more\n'
            '/remember <code>-p &lt;name&gt; -d &lt;date&gt; '
            '[-l &lt;location&gt;]</code> - '
            'I\'ll remember this birthday and will notify you a couple '
            'of days in advance\n'
            '/forget <code>&lt;number from list&gt;</code> '
            '- I\'ll forget specified birthday'
            '/list - I\'ll show you all dates I remember\n',

            parse_mode='HTML')

    @dispatcher.handler('remember')
    def add_date(message: Message):
        user = data['users'].get(str(message.from_user.id))

        if not user:
            message.answer(f'Who are you? I do not remember you. '
                           f'Please type /start to start')

            return

        args = message.arguments

        if not args:
            message.answer('This command requires arguments! '
                           'If you are not sure, please type /help')
            return

        person = args.get('-p')

        if not person:
            message.answer(f'You have missed the name of the person\\. '
                           f'Please use `-p <name>` to specify',

                           parse_mode='MarkdownV2')
            return

        date = args.get('-d')

        if not date:
            message.answer(f'You have missed the date of the event\\. '
                           f'Please use `-d <DD.MM.YYYY>` to specify',

                           parse_mode='MarkdownV2')
            return

        try:
            date = datetime.strptime(date, '%d.%m.%Y')
        except Exception as e:
            print(e)

            message.answer(f'Your date does not seem right to me\\. '
                           f'Please make sure it is in `DD.MM.YYYY` format '
                           f'\\(e\\.g\\. `06.03.2000`\\)',

                           parse_mode='MarkdownV2')
            return

        address = args.get('-l')

        user['dates'].append(functions.make_date(
            person, date.strftime(data['config']['date_format']), address)
        )

        message.answer(f'I\'ll remember this!')

    @dispatcher.handler('list')
    def display_list(message: Message):
        user = data['users'].get(str(message.from_user.id))

        if not user:
            message.answer(f'Who are you? I do not remember you. '
                           f'Please type /start to start')
            return

        if not user['dates']:
            message.answer(f'I do not remember any birthday for you. '
                           f'Please use /remember to add them')
            return

        text = 'Here are birthdays you\'ve asked me to remember:\n\n'

        for i, date in enumerate(user['dates']):

            if date['address']:
                pattern = '{person} - {date} - {address}\n'

            else:
                pattern = '{person} - {date}\n'

            text += f'{i+1}. ' + pattern.format(
                person=date['person'],
                address=date['address'],
                date=date['date']
            )

        text += '\nIf you want me to forget some birthday, ' \
                'please use /forget and its number.'

        message.answer(text)

    @dispatcher.handler('forget')
    def remove_date(message: Message):
        user = data['users'].get(str(message.from_user.id))

        if not user:
            message.answer(f'Who are you? I do not remember you. '
                           f'Please type /start to start')
            return

        if not user['dates']:
            message.answer(f'I do not remember any birthday for you. '
                           f'Please use /remember to add them first')
            return

        if not message.rest:
            message.answer(f'Please specify number of birthday you want me '
                           f'to forget: /forget `<number>`',

                           parse_mode='MarkdownV2')
            return

        try:
            number = int(message.rest)

        except ValueError:
            message.answer(f'"{message.rest}" does not seem like a number to '
                           f'me. Please try again')
            return

        if number > len(user['dates']):
            message.answer(f'Your number is too big! '
                           f'I only remember {len(user["dates"])} birthdays')
            return

        d = user['dates'].pop(number - 1)

        message.answer(f'I removed birthday of {d["person"]} from my list')

    @dispatcher.handler(unknown=True)
    def unknown_command(message: Message):
        message.answer('I don\'t know this command :(. Please type '
                       '/help and I\'ll show you available commands')

    @dispatcher.handler()
    def echo(message: Message):
        message.answer(f'You said: "{message.text}"')
