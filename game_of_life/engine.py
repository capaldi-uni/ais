"""Core game classes. Contain common game routines."""
import pygame
import pygame as pg

from abc import ABC, abstractmethod

from typing import Optional


class Game(object):
    """Game controller"""

    def __init__(self, size=(640, 480), fps=60,
                 starting_scene: "Optional[Scene]" = None):
        """
        Create new game with given window size, framerate and starting_scene.

        :param size: Window size
        :param fps: Desired framerate
        :param starting_scene:
        """

        self.screen = pg.display.set_mode(size, pygame.RESIZABLE)

        self.running = False

        self.clock = pg.time.Clock()
        self.fps = fps

        self.tpu = 1000 / fps

        self.scene = starting_scene

        if self.scene:
            self.scene.init(self)

    def set_scene(self, new_scene: "Scene"):
        """
        Set new scene as current.

        :param new_scene: Scene to set as current.
        """

        self.scene = new_scene
        self.scene.init(self)

    def quit(self):
        """Stop game loop."""

        self.running = False

    def dispatch_events(self):
        """Get and dispatch pygame events."""

        for event in pg.event.get():
            if event.type == pg.QUIT:
                self.running = False

            if event.type == pg.WINDOWRESIZED:
                self.screen = pg.display.set_mode(
                    (event.x, event.y), pg.RESIZABLE)

                self.draw(forced=True)

            else:
                if self.scene:
                    self.scene.process_event(event)

    def update(self):
        """Call scene update."""

        if self.scene:
            self.scene.update(self.tpu)

    def draw(self, *, forced=False):
        """Draw next frame."""

        if self.scene:
            self.scene.display(self.screen, forced=forced)

        pg.display.flip()

    def run(self):
        """Run main loop."""

        self.running = True

        t = 0

        while self.running:
            t += self.clock.tick(self.fps)

            self.dispatch_events()

            while t >= self.tpu:
                self.update()
                t -= self.tpu

            self.draw()


class Scene(ABC):
    """Abstract scene to be used in `Game` class."""
    game = None
    reinit = False

    def __init__(self, next_scene: "Optional[Scene]" = None):
        """
        Create new scene.

        :param next_scene: Scene to be activated when finished.
        """
        self.next_scene = next_scene

    def init(self, game: Game):
        """
        Initialize scene on given game.

        :param game: Master game.
        """
        self.game = game

    @abstractmethod
    def update(self, dt: float):
        """
        Perform update.

        :param dt: Time since last update in milliseconds.
        """
        pass

    @abstractmethod
    def display(self, screen: pg.surface.Surface, *, forced=False):
        """
        Draw scene on screen.

        :param screen: Screen to draw on.
        :param forced: Is display forced outside of update loop.
        """
        pass

    @abstractmethod
    def process_event(self, event: pg.event.Event):
        """
        Process incoming event.

        :param event: Event to process.
        """
        pass

    def finish(self):
        """End scene lifecycle and activate next scene or quit the game."""

        if self.game:
            if self.next_scene:
                self.game.set_scene(self.next_scene)

            else:
                self.game.quit()
