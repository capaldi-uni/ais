"""Game of Life scene."""

from . import color
from .engine import Scene
from .painters import GridPainter

import numpy as np
import pygame as pg

from typing import Tuple


class Life(Scene):
    """Game of Life scene"""

    PAUSE = 1
    PLAY = 0

    field: np.ndarray

    def __init__(self, field_size: Tuple[int, int], init_speed: int = 5,
                 random_fill: bool = False, random_fill_rate: float = 0.2):
        """
        Create new scene with given parameters.

        :param field_size: Size of field in cells
        :param init_speed: Initial speed of game (updates per seconds)
        :param random_fill: Whether to fill field with random cells
        :param random_fill_rate: Fraction of alive cells in random fill
        """

        super().__init__()

        self.speed = init_speed
        self.delay = 1 / self.speed
        self.elapsed = 0

        self.field_size = field_size
        self.field = np.zeros(self.field_size)

        self.random_fill = random_fill
        self.fill_rate = random_fill_rate

        self.painter = GridPainter(self.field)

        self.state = self.PAUSE

        self.drawing = False
        self.draw_material = 0

        self.dragging = False

    def init(self, game):

        pg.display.set_caption('Conway\'s Game of Life')

        if not self.reinit:
            super().init(game)

            self.painter.calculate_screen_pos(game.screen.get_size())

            # Set cells to random state
            if self.random_fill:
                fill = int(10 * self.fill_rate)
                a = fill * (1,) + (10 - fill) * (0, )

                # Replace contents of old array rather than create new
                # (so painter's reference is still valid)
                self.field[:, :] = np.random.choice(a, self.field.shape)

            self.reinit = True

    def process_event(self, event):

        if event.type == pg.KEYUP:
            # Pause/unpause on `Space`
            if event.key == pg.K_SPACE:
                self.state = 1 - self.state

            # Clear field on `X`
            elif event.key == pg.K_x:
                self.clear_field()

        elif event.type == pg.MOUSEBUTTONDOWN:
            # Draw on `LMB` or `RMB`
            if event.button == 1 or event.button == 3:
                self.start_drawing(event.button)

            # Drag field on `MMB` (wheel)
            elif event.button == 2:
                self.start_drag()

        elif event.type == pg.MOUSEBUTTONUP:
            # Stop draw
            if event.button == 1 or event.button == 3:
                self.stop_drawing()

            # Stop drag
            elif event.button == 2:
                self.stop_drag()

        elif event.type == pg.MOUSEMOTION:
            # Handle mouse movements when dragging
            if self.dragging:
                self.drag(event)

        elif event.type == pg.MOUSEWHEEL:
            # Zoom on scrolling
            self.painter.zoom(event.y, pg.mouse.get_pos(),
                              self.game.screen.get_size())

    def start_drawing(self, button: int):
        """
        Enter drawing state. In drawing state cells under cursor will change
        state according to mouse button that was pressed.

        LMB - alive, RMB - dead.

        :param button: Button that was pressed
        """

        if self.state == self.PAUSE:
            self.drawing = True

            # State depends on mouse button
            if button == 1:
                self.draw_material = 1
            else:
                self.draw_material = 0

    def stop_drawing(self):
        """Exit drawing state."""

        self.drawing = False

    def start_drag(self):
        """
        Enter field dragging state. In this state field will move
        along with mouse cursor.
        """

        self.dragging = True

    def stop_drag(self):
        """Exit dragging state."""

        self.dragging = False

    def drag(self, event: pg.event.Event):
        """
        Move field along with cursor.

        :param event: Mouse movement event.
        """

        self.painter.move(*event.rel, self.game.screen.get_size())

    def clear_field(self):
        """Set all cells on field to dead state."""

        self.field[:, :] = np.zeros_like(self.field)

    def update(self, dt):

        if self.state != self.PAUSE:
            self.elapsed += dt / 1000

            if self.elapsed > self.delay:
                new_field = np.copy(self.field)

                # Calculate next generation
                for i in range(self.field.shape[0]):
                    for j in range(self.field.shape[1]):

                        n = count_neighbours(self.field, i, j)

                        if self.field[i][j] == 0 and n == 3:
                            new_field[i][j] = 1

                        elif not (2 <= n <= 3):
                            new_field[i][j] = 0

                self.field[:, :] = new_field

                self.elapsed -= self.delay

        else:
            # Draw when paused
            if self.drawing:
                pos = self.painter.screen_to_surface(pg.mouse.get_pos())

                if self.painter.is_on_surface(pos):
                    x, y = self.painter.get_grid_pos(pos)

                    self.field[x][y] = self.draw_material

    def display(self, screen: pg.surface.Surface, *, forced=False):

        screen.fill(color.black)    # Prevent windows xp solitaire

        if forced:
            # Make sure field is positioned properly after resize
            self.painter.move(0, 0, screen.get_size())

        self.painter.draw(screen)


def count_neighbours(arr: np.ndarray, i: int, j: int) -> int:
    """
    Get count of alive neighbours for cell of given array with given coordinates

    :param arr: Array to check cells in.
    :param i: First coordinate of cell.
    :param j: Second coordinate of cell.
    :return: Number of alive neighbours.
    """

    w, h = arr.shape

    i_s = i - 1, i, (i + 1) % w
    j_s = j - 1, j, (j + 1) % h

    return sum(arr[ix][jx] for ix in i_s for jx in j_s) - arr[i][j]
