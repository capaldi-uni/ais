"""Run the game."""

from .engine import Game
from .life import Life

import pygame as pg

WIDTH = 58
HEIGHT = 43

import argparse

# Set up argument parser
parser = argparse.ArgumentParser(
    prog='python -m game_of_life',
    description='Conway\'s Game of Life implemented using python and pygame'
)

parser.add_argument(
    '--width', type=int, default=WIDTH,
    help='Width of game field in cells'
)

parser.add_argument(
    '--height', type=int, default=HEIGHT,
    help='Height of game field in cells'
)

a = parser.parse_args()

# Create and run game
g = Game((1280, 720),

         starting_scene=Life(
             (a.width, a.height),
             random_fill=True,
             random_fill_rate=0.1)
         )
g.run()

pg.quit()
