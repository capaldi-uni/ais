"""Conway's Game of Life implementation using pygame."""

from types import SimpleNamespace

import os
# Hide numpy hello message
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"

import pygame as pg
# Initialization of pygame modules
pg.init()

# Namespace with all available in pygame named colors
color = SimpleNamespace(**{name.replace(' ', '_'): pg.color.Color(name)
                           for name in pg.colordict.THECOLORS},

                        make=pg.color.Color)
