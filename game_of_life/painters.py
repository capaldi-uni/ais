"""Classes to handle drawing."""

from . import color

import pygame as pg
import numpy as np

from typing import Tuple


class GridPainter(object):
    """Draws Game of Life's grid on screen"""

    zoom_levels = [10, 20, 40, 80]

    zoom_level: int = None
    cell_size: int = None
    cell_space: int = None
    cell_offset: int = None

    surf: pg.surface.Surface = None

    def __init__(self, grid: np.ndarray, origin: Tuple[int, int] = (0, 0),
                 default_zoom: int = 0):
        """
        Create painter for given grid.

        :param grid: Grid to paint.
        :param origin: Coordinates where to paint the grid.
        :param default_zoom: Default zoom level.
        """

        self.grid = grid

        self.set_zoom_level(default_zoom)
        self.make_surface()

        self.screen_pos = origin

    def make_surface(self):
        """Create surface depending on current cell sizes."""
        surf_size = \
            self.grid.shape[0] * self.cell_offset + self.cell_space, \
            self.grid.shape[1] * self.cell_offset + self.cell_space

        self.surf = pg.surface.Surface(surf_size)

    def set_zoom_level(self, l):
        """
        Try set current zoom level to `l`.
        It will be clamped between 0 and zoom_levels count

        :param l: Desired zoom level
        """

        # Clamp value inbounds
        self.zoom_level = min(len(self.zoom_levels) - 1, max(0, l))

        # Calculate sized according to zoom
        self.cell_size = self.zoom_levels[self.zoom_level]
        self.cell_space = max(1, self.cell_size // 10)
        self.cell_offset = self.cell_size + self.cell_space

    def zoom(self, y, pos, screen_shape):
        """
        Change current zoom level by `y`

        :param y: How much to change zoom level.
        :param pos: Mouse position at the moment of zoom.
        :param screen_shape: Size of screen grid will be displayed on.
        """

        l = self.zoom_level
        self.set_zoom_level(l + y)

        # Don't waste time if level didn't change
        if l != self.zoom_level:
            # Make new surface (since size changed)
            self.make_surface()

            # Adjust to moue position and new size
            rat = self.zoom_levels[l] / self.zoom_levels[self.zoom_level]

            self.screen_pos = (
                int((self.screen_pos[0] - pos[0]) / rat + pos[0]),
                int((self.screen_pos[1] - pos[1]) / rat + pos[1])
            )

            # Force limits recalculation
            self.move(0, 0, screen_shape)

    def move(self, rel_x, rel_y, screen_shape, border=0):
        """
        Move grid on screen.

        :param rel_x: Movement magnitude along x axis.
        :param rel_y: Movement magnitude along y axis.
        :param screen_shape: Size of screen grid will be displayed at.
        :param border: Size of minimum free space on the edges of screen (if 0,
        when grid doesn't fit screen, won't allow free space between grid and
        edge of screen)
        """

        x, y = self.screen_pos

        srx, sry = self.surf.get_size()

        # Calculate new position while not going out of bounds too much
        self.screen_pos = (
            min(max(x + rel_x, min(screen_shape[0] - srx - border, 0)),
                max(screen_shape[0], srx + border) - srx),

            min(max(y + rel_y, min(screen_shape[1] - sry - border, 0)),
                max(screen_shape[1], sry + border) - sry)
        )

    def calculate_screen_pos(self, screen_shape):
        """
        Calculate appropriate position for field to be displayed on screen.

        :param screen_shape: Shape of screen grid will be displayed on.
        """

        # If surface > screen, place in the left corner
        # Else - in the middle of a screen
        self.screen_pos = \
            max(0, (screen_shape[0] - self.surf.get_width()) // 2), \
            max(0, (screen_shape[1] - self.surf.get_height()) // 2)

    def get_grid_pos(self, surface_pos: Tuple[int, int]) -> Tuple[int, int]:
        """
        Calculate grid indices of cell displayed at `surface_pos` coordinates
        on surface.

        :param surface_pos: Position of cell on surface.
        :return: Grid indices of cell.
        """

        x, y = surface_pos

        x -= self.cell_space
        y -= self.cell_space

        x //= self.cell_offset
        y //= self.cell_offset

        x = max(0, x)
        y = max(0, y)

        return x, y

    def screen_to_surface(self, pos: Tuple[int, int]) -> Tuple[int, int]:
        """
        Convert position on screen to position on surface.

        :param pos: Screen position.
        :return: Surface position.
        """

        return pos[0] - self.screen_pos[0], pos[1] - self.screen_pos[1]

    def is_on_surface(self, pos: Tuple[int, int]) -> bool:
        """
        Check if given position is on surface.

        :param pos: Position to check.
        :return: Whether position is on surface.
        """

        return \
            0 <= pos[0] < self.surf.get_width() and \
            0 <= pos[1] < self.surf.get_height()

    def draw(self, screen: pg.surface.Surface):
        """
        Draw grid on given screen.

        :param screen: Screen to draw grid on.
        """

        self.surf.fill(color.grey10)

        # Calculate bounds to draw in (don't need to draw what wouldn't be seen)
        zero = self.screen_to_surface((0, 0))
        opposite = self.screen_to_surface(screen.get_size())

        if self.is_on_surface(zero):
            low_x, low_y = self.get_grid_pos(zero)

        else:
            low_x, low_y = 0, 0

        if self.is_on_surface(opposite):
            high_x, high_y = self.get_grid_pos(opposite)
            high_x += 1
            high_y += 1

        else:
            high_x, high_y = self.grid.shape

        # Draw all visible cells
        for i in range(low_x, high_x):
            for j in range(low_y, high_y):
                # Determine cell color
                cell_color = color.white if self.grid[i][j] else color.black

                # Calculate rect parameters
                rect = \
                    self.cell_space + i * self.cell_offset, \
                    self.cell_space + j * self.cell_offset, \
                    self.cell_size, self.cell_size

                pg.draw.rect(self.surf, cell_color, rect)

        screen.blit(self.surf, self.screen_pos)
